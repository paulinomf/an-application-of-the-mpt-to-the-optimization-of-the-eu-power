# An application of the Modern Portfolio Theory to the optimization of the European Union power generation mix from an environmental perspective #

This repository contains the code in R used along the models presented in the Doctoral Thesis of Paulino Martinez-Fernandez. The code is contained in the file generation.portfolios.r. The functions included in the file are:

1. gmv.portfolio: calculates the GMV portfolio given at least the average values (costs or emission factors) and the variances-covariances matrix.
2. extreme.portfolio: calculates the other extreme portfolio of either the efficient frontier or the feasible frontier.
3. efficient.portfolio: calculates the risk of an efficient portfolio given its cost or emission factor.
4. efficient.frontier: calculates a collection of efficient portfolios between the GMV and the corresponding extreme portfolio (depending of whether we want to calculate the efficient frontier or the feasible frontier).
5. solve.model: calculates the whole model (GMV, GMC or GMV, efficient frontier and feasible frontier).
6. print.portfolio: shows the risk and the expected cost or emission factor and the weights of a specific portfolio.
7. plot.portfolio: plot the weights of a specific portfolio in a bar plot or in a pie chart.
8. print.Markowitz: shows the risks and the expected costs or emission factors of the portfolios in the efficient frontier or in the feasible frontier.
9. summary.Markowitz: calculates the tangency portfolio and appends its compositio to the information about the portfolios in the efficient or in the feasible frontier.
10. tangency.portfolio: calculates the tangency.portfolio given a risk.free asset cost or emission.factor.
11. print.summary.Markowitz: shows the risks and the expected costs or emission factors of the portfolios in the efficient frontier or in the feasible frontier and the tangency portfolio.
12. plot.model: plots a model in a risk-cost or in a risk-emission coordinates plane.
13. distanceToEff: calculate the Euclidean distance from a given portfolio to the efficient frontier. The line used to measure the distance is the line joining the portfolio and the coordinates origin point.
14. errorhandler: internal function to handle any eventual error arising from the program execution.