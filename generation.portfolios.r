# Classes in this file:
# portfolio
# Markowitz
# model
# summary.Markowitz
# dist2eff

stopifnot("package:NlcOptim" %in% search() || require("NlcOptim",quietly = TRUE))
stopifnot("package:sp" %in% search() || require("sp",quietly = TRUE))
stopifnot("package:rgeos" %in% search() || require("rgeos",quietly = TRUE))

require(tools)

gmv.portfolio <- function(ev, vcv, addRestrs=NULL, addLimits=NULL, 
                          addEqRestrs=NULL, addEqLimits=NULL, confun=NULL,
                          shorts=FALSE) {
  # Compute global minimum variance portfolio
  #
  # inputs:
  # ev  	    N x 1 vector of expected values (returns or costs)
  # vcv   		N x N return covariance matrix
  # addRestrs   each additional restriction is a Nx1 vector of coefficients for the variables
  # addLimits   for every additional restriction we must pass a number with the limit (>=)
  # addEqRestrs each additional equality restriction is a Nx1 vector of coefficients for the variables
  # addEqLimits for every additional restriction we must pass a number with the limit (=)
  # confun      Nonlinear constraint function. Return a ceq vector and a c vector as nonlinear equality constraints and an inequality constraints.
  # shorts      logical, allow shorts is TRUE
  #
  # output is portfolio object with the following elements:
  # call	    original function call
  # ev			portfolio expected value (return or cost)
  # sd			portfolio standard deviation
  # weights		N x 1 vector of portfolio weights
  
  call <- match.call()
  
  #
  # check for valid inputs
  #
  asset.names <- names(ev)
  if (is.null(asset.names)) {
    asset.names <- rownames(ev)
  }
  ev <- as.matrix(ev, ncol=1) # assign names if none exist
  vcv <- as.matrix(vcv)
  N <- length(ev)
  if(N != nrow(vcv))
    stop("invalid inputs: the number of rows in expected values vector and in VCV matrix must be equal")
  if(any(diag(chol(vcv)) <= 0))
    warning("Covariance matrix not positive definite")
  # remark: could use generalized inverse if vcv is positive semi-definite
  
  #
  # x vector and objective function
  #
  x <- as.matrix(rep.int(0, N), ncol=1)
  
  obj.f <- function(x) {
    # Portfolio SD
    sqrt(t(x) %*% vcv %*% x)
  }
  
  #
  # inequality constraints
  #
  
  if (shorts) {
    A = NULL
    B = NULL
  } else if (!shorts) {
    # Non-negativity constraints
    A <- -diag(1, N, N)
    B <- as.matrix(-rep.int(0, N), nrow=1)
  } else {
    stop("shorts needs to be logical. For no-shorts, shorts=FALSE.")
  }
  
  if(!is.null(addRestrs)) {
    if (is.null(A)) {
      A <- addRestrs
      B <- as.matrix(addLimits, nrow=1)
    } else {
      A <- rbind(A, addRestrs)
      B <- rbind(B, as.matrix(addLimits, nrow=1))
    }
  }
  
  #
  # equality constraints
  #
  
  # Participations must sum up to 1
  Aeq <- t(rep.int(1, N))
  Beq <- 1
  
  if(!is.null(addEqRestrs)) {
    if (is.null(Aeq)) {
      Aeq <- addEqRestrs
      Beq <- as.matrix(addEqLimits, ncol=1)
    } else {
      Aeq <- rbind(Aeq, addEqRestrs)
      Beq <- rbind(Beq, as.matrix(addEqLimits, ncol=1))
    }
  }
  
  result <- tryCatch({
      solnl(x, obj.f, A=A, B=B, Aeq=Aeq, Beq=Beq, confun=confun)
    }, error=function(err) {
      errorHandler(err, call, x, A, B, Aeq, Beq, confun)
      stop(geterrmessage())
    })

  gmv.w <- as.vector(round(result$par, 6))
  names(gmv.w) <- asset.names

  gmv.ev <- crossprod(gmv.w, ev)
  gmv.sd <- result$fn
  gmv.port <- list("call" = call,
                   "ev" = as.vector(gmv.ev),
                   "sd" = as.vector(gmv.sd),
                   "weights" = gmv.w,
                   "HH" = sum(gmv.w^2))
  class(gmv.port) <- "portfolio"
  gmv.port
}

extreme.portfolio <- function(ev, vcv, maximum=FALSE, addRestrs = NULL, addLimits = NULL,
                              addEqRestrs=NULL, addEqLimits=NULL, confun=NULL,
                              shorts=FALSE) {
  # Compute global min/max return or cost portfolio
  #
  # inputs:
  # ev    		N x 1 vector of expected values (returns or costs)
  # vcv   		N x N return covariance matrix
  # maximum   FALSE (default) for searching the lower extreme portfolio
  # addRestrs each additional restriction is a Nx1 vector of coefficients for the variables
  # addLimits for every additional restriction we must pass a number with the limit (>=)
  # addEqRestrs each additional equality restriction is a Nx1 vector of coefficients for the variables
  # addEqLimits for every additional restriction we must pass a number with the limit (=)
  # confun      Nonlinear constraint function. Return a ceq vector and a c vector as nonlinear equality constraints and an inequality constraints.
  #
  # output is portfolio object with the following elements
  # call			original function call
  # ev				portfolio expected value (return or cost)
  # sd				portfolio standard deviation
  # weights		N x 1 vector of portfolio weights
  
  call <- match.call()
  
  asset.names <- names(ev)
  if (is.null(asset.names)) {
    asset.names <- rownames(ev)
  }
  
  N <- length(ev)
  
  #
  # compute extreme portfolio
  #
  x <- rep.int(0, N)
  
  obj.f <- function(x) {
    # Portfolio EV
    if (maximum) {
      -t(x) %*% ev
    } else {
      t(x) %*% ev
    }
  }
  
  #
  # inequality constraints
  #
  
  if (shorts) {
    A = NULL
    B = NULL
  } else if (!shorts) {
    # Non-negativity constraints
    A <- -diag(1, N, N)
    B <- as.matrix(-rep.int(0, N), nrow=1)
  } else {
    stop("shorts needs to be logical. For no-shorts, shorts=FALSE.")
  }
  
  if(!is.null(addRestrs)) {
    if (is.null(A)) {
      A <- addRestrs
      B <- as.matrix(addLimits, nrow=1)
    } else {
      A <- rbind(A, addRestrs)
      B <- rbind(B, as.matrix(addLimits, nrow=1))
    }
  }
  
  #
  # equality constraints
  #
  
  # Participations must sum up to 1
  Aeq <- t(rep.int(1, N))
  Beq <- 1
  
  if(!is.null(addEqRestrs)) {
    if (is.null(Aeq)) {
      Aeq <- addEqRestrs
      Beq <- as.matrix(addEqLimits, ncol=1)
    } else {
      Aeq <- rbind(Aeq, addEqRestrs)
      Beq <- rbind(Beq, as.matrix(addEqLimits, ncol=1))
    }
  }
  
  result <- tryCatch({
      solnl(x, obj.f, A=A, B=B, Aeq=Aeq, Beq=Beq, confun=confun)
    }, error=function(err) {
      errorHandler(err, call, x, A, B, Aeq, Beq, confun)
      stop(geterrmessage())
    })

  extr.w <- as.vector(round(result$par, 6))
  names(extr.w) <- asset.names
  
  if (maximum) {
    extr.ev <- -result$fn
  } else {
    extr.ev <- result$fn
  }
  
  extr.sd <- sqrt(t(result$par) %*% vcv %*% result$par)
  extr.port <- list("call" = call,
                    "ev" = as.vector(extr.ev),
                    "sd" = as.vector(extr.sd),
                    "weights" = extr.w,
                    "HH" = sum(extr.w^2))
  class(extr.port) <- "portfolio"
  extr.port
}

efficient.portfolio <-  function(ev, vcv, target.value, 
                                 addRestrs=NULL, addLimits=NULL, 
                                 addEqRestrs=NULL, addEqLimits=NULL,
                                 confun=NULL, shorts=FALSE) {
  # compute minimum variance portfolio subject to target return in terms of inequality
  #
  # inputs:
  # ev					  N x 1 vector of expected values
  # vcv  			    N x N covariance matrix of returns
  # target.value  scalar, target expected value
  # addRestrs     each additional restriction is a Nx1 vector of 0s and 1s (the 1 is in the variable/s affected by the restriction)
  # addLimits     for every additional restriction we pass a number with the limit (>=)
  # addEqRestrs each additional equality restriction is a Nx1 vector of coefficients for the variables
  # addEqLimits for every additional restriction we must pass a number with the limit (=)
  # confun      Nonlinear constraint function. Return a ceq vector and a c vector as nonlinear equality constraints and an inequality constraints.
  # shorts        logical, allow shorts is TRUE
  #
  # output is portfolio object with the following elements
  # call				    original function call
  # ev					    portfolio expected value
  # sd					    portfolio standard deviation
  # weights			    N x 1 vector of portfolio weights
  #
  call <- match.call()
  
  #
  # check for valid inputs
  #
  asset.names <- names(ev)
  if (is.null(asset.names)) {
    asset.names <- rownames(ev)
  }
  
  ev <- as.vector(ev)					# assign names if none exist
  N <- length(ev)
  vcv <- as.matrix(vcv)
  if(N != nrow(vcv))
        stop("invalid inputs: the number of rows in expected values vector and in VCV matrix must be equal")
  if(any(diag(chol(vcv)) <= 0))
    stop("Covariance matrix not positive definite")
  # remark: could use generalized inverse if vcv is positive semidefinite
  
  x <- rep.int(0, N)
  
  obj.f <- function(x) {
    # Portfolio SD
    sqrt(t(x) %*% vcv %*% x)
  }
  
  if (shorts) {
    A = NULL
    B = NULL
  } else if (!shorts) {
    # Non-negativity constraints
    A <- -diag(1, N, N)
    B <- as.matrix(-rep.int(0, N), nrow=1)
  } else {
    stop("shorts needs to be logical. For no-shorts, shorts=FALSE.")
  }
  
  if(!is.null(addRestrs)) {
    if (is.null(A)) {
      A <- addRestrs
      B <- as.matrix(addLimits, nrow=1)
    } else {
      A <- rbind(A, addRestrs)
      B <- rbind(B, as.matrix(addLimits, nrow=1))
    }
  }
  
  # Participations must sum up to 1
  Aeq <- t(rep.int(1, N))
  Beq <- 1
  
  # Target value (return or cost)
  Aeq <- rbind(Aeq, ev)
  Beq <- rbind(as.matrix(Beq, ncol=1), target.value)
  
  if(!is.null(addEqRestrs)) {
    if (is.null(Aeq)) {
      Aeq <- addEqRestrs
      Beq <- as.matrix(addEqLimits, ncol=1)
    } else {
      Aeq <- rbind(Aeq, addEqRestrs)
      Beq <- rbind(Beq, as.matrix(addEqLimits, ncol=1))
    }
  }
  
  # if(ncol(B) > 1) {
  #   stop()
  # }
  # if(NROW(Beq) > 1 | NCOL(Beq) > 1) {
  #   stop()
  # }
  
  #
  # compute efficient portfolio
  #
  result <- tryCatch({
      solnl(x, obj.f, A=A, B=B, Aeq=Aeq, Beq=Beq, confun=confun)
    }, error=function(err) {
      errorHandler(err, call, x, A, B, Aeq, Beq, confun)
      stop(geterrmessage())
    })
  
  #
  # compute portfolio expected returns and variance
  #
  eff.port.w <- as.vector(round(result$par, 6))
  names(eff.port.w) <- asset.names
  eff.port.ev <- crossprod(ev, eff.port.w)
  eff.port.sd <- result$fn
  ans <- list("call" = call,
              "ev" = as.vector(eff.port.ev),
              "sd" = as.vector(eff.port.sd),
              "weights" = eff.port.w,
              "HH" = sum(eff.port.w^2)) 
  class(ans) <- "portfolio"
  ans
}

efficient.frontier <- function(ev, vcv, direction=-1, 
                               addRestrs=NULL, addLimits=NULL, 
                               addEqRestrs=NULL, addEqLimits=NULL,
                               confun=NULL, shorts=FALSE, nport=20, 
                               alpha.min=-0.5, alpha.max=1.5, 
                               gmv.port = NULL, verbose = FALSE)
{
  # Compute efficient/feasible frontier
  #
  # inputs:
  # ev			  N x 1 vector of expected values (returns or costs)
  # vcv	  N x N return covariance matrix
  # direction how to build the  frontier: going to the max ev (1) or going to the min ev (-1, default)
  # addRestrs each additional restriction is a Nx1 vector of coefficients for the variables
  # addLimits for every additional restriction we must pass a number with the limit (>=)
  # addEqRestrs each additional equality restriction is a Nx1 vector of coefficients for the variables
  # addEqLimits for every additional restriction we must pass a number with the limit (=)
  # confun      Nonlinear constraint function. Return a ceq vector and a c vector as nonlinear equality constraints and an inequality constraints.
  # nport		  scalar, number of efficient portfolios to compute
  # shorts    logical, allow shorts is TRUE
  #
  # output is a Markowitz object with the following elements
  # call		  captures function call
  # ev			  nport x 1 vector of expected returns on efficient porfolios
  # sd			  nport x 1 vector of std deviations on efficient portfolios
  # weights 	nport x N matrix of weights on efficient portfolios 
  
  call <- match.call()
  
  #
  # check for valid inputs
  #
  asset.names <- names(ev)
  ev <- as.vector(ev)
  N <- length(ev)
  vcv <- as.matrix(vcv)
  if(N != nrow(vcv))
        stop("invalid inputs: the number of rows in expected values vector and in VCV matrix must be equal")
  if(any(diag(chol(vcv)) <= 0))
    stop("Covariance matrix not positive definite")
  
  #
  # create portfolio names
  #
  port.names <- rep("port", nport)
  ns <- seq(1, nport)
  port.names <- paste(port.names, ns)
  
  #
  # compute global minimum variance portfolio
  #
  if(is.null(gmv.port)) {
    tryCatch({
      gmv.port <- gmv.portfolio(ev, vcv, addRestrs, addLimits, 
                                addEqRestrs, addEqLimits, confun, shorts)
    }, warning=function(warn) {
      warning(warn)             
      return(NA)
    }, error=function(err){
      stop(err)                         
      return(NA)
    })
  }
  gmv.w <- gmv.port$weights
  if (verbose) {
   cat(paste("GMV computed: ", gmv.port$sd, gmv.port$ev, "\r\n"))
  }
  
  #
  # compute extreme portfolio
  #
  if(direction==1) {
    maximum=TRUE
  } else {
    maximum=FALSE
  }
  extr.port <- extreme.portfolio(ev, vcv, maximum, addRestrs, addLimits, addEqRestrs, addEqLimits, confun)
  extr.w <- extr.port$weights
  if(verbose) {
    if (maximum) {
      cat(paste("Upper extreme portfolio computed: ", extr.port$sd, extr.port$ev, "\r\n"))
    } else {
      cat(paste("Lower extreme portfolio computed: ", extr.port$sd, extr.port$ev, "\r\n"))
    }
  }
  
  we.mat <- matrix(0, nrow=nport, ncol=N)
  we.mat[1,] <- gmv.w
  
  we.mat[nport,] <- extr.w
  ev.constr <- as.vector(seq(from=gmv.port$ev, to=extr.port$ev, length=nport))
  if(verbose){
    cat("Value limits computed:\r\n", paste(ev.constr), "\r\n")
  } 
  
  for(i in 2:(nport-1)) {
    tryCatch({
      ep <- efficient.portfolio(ev, vcv, ev.constr[i], addRestrs, addLimits, 
                                addEqRestrs, addEqLimits, confun, shorts)
      if(verbose) {
        cat(paste("Efficient portfolio computed for value constraint:", ev.constr[i], "\r\n"))
      }
      we.mat[i, ] <- ep$weights
    }, error=function(err){
      errorHandler(err, call, x, A, B, Aeq, Beq, confun)
      stop(geterrmessage())
    })
  }
  
  names(ev.constr) <- port.names
  vcv.e <- we.mat %*% vcv %*% t(we.mat) # cov mat of efficient portfolios
  sd.e <- sqrt(diag(vcv.e))	# std devs of efficient portfolios
  sd.e <- as.vector(sd.e)
  names(sd.e) <- port.names
  dimnames(we.mat) <- list(port.names, asset.names)
  
  # 
  # summarize results
  #
  ans <- list("call" = call,
              "ev" = ev.constr,
              "sd" = sd.e,
              "weights" = we.mat)
  class(ans) <- "Markowitz"
  ans
}

tangency.portfolio <- function(er, cov.mat, risk.free, shorts=FALSE) {
  # compute tangency portfolio
  #
  # inputs:
  # er				   N x 1 vector of expected returns
  # cov.mat		   N x N return covariance matrix
  # risk.free		 scalar, risk-free rate
  # shorts          logical, allow shorts is TRUE
  #
  # output is portfolio object with the following elements
  # call			  captures function call
  # er				  portfolio expected return
  # sd				  portfolio standard deviation
  # weights		 N x 1 vector of portfolio weights
  
  library(quadprog) # TODO: change to NlcOptim solnl function
  
  call <- match.call()
  
  #
  # check for valid inputs
  #
  asset.names <- names(er)
  if(risk.free < 0)
    stop("Risk-free rate must be positive")
  er <- as.vector(er)
  cov.mat <- as.matrix(cov.mat)
  N <- length(er)
  if(N != nrow(cov.mat))
    stop("invalid inputs")
  if(any(diag(chol(cov.mat)) <= 0))
    stop("Covariance matrix not positive definite")
  # remark: could use generalized inverse if cov.mat is positive semi-definite
  
  #
  # compute global minimum variance portfolio
  #
  gmin.port <- gmv.portfolio(er, cov.mat, shorts=shorts)
  if(gmin.port$ev < risk.free)
    stop("Risk-free rate greater than avg return on global minimum variance portfolio")
  
  # 
  # compute tangency portfolio
  #
  if(shorts==TRUE){
    cov.mat.inv <- solve(cov.mat)
    w.t <- cov.mat.inv %*% (er - risk.free) # tangency portfolio
    w.t <- as.vector(w.t/sum(w.t))	# normalize weights
  } else if(shorts==FALSE){
    Dmat <- 2*cov.mat
    dvec <- rep.int(0, N)
    er.excess <- er - risk.free
    Amat <- cbind(er.excess, diag(1,N))
    bvec <- c(1, rep(0,N))
    result <- solve.QP(Dmat=Dmat,dvec=dvec,Amat=Amat,bvec=bvec,meq=1)
    w.t <- round(result$solution/sum(result$solution), 6)
  } else {
    stop("Shorts needs to be logical. For no-shorts, shorts=FALSE.")
  }
  
  names(w.t) <- asset.names
  er.t <- crossprod(w.t,er)
  sd.t <- sqrt(t(w.t) %*% cov.mat %*% w.t)
  tan.port <- list("call" = call,
                   "ev" = as.vector(er.t),
                   "sd" = as.vector(sd.t),
                   "weights" = w.t)
  class(tan.port) <- "portfolio"
  tan.port
}

solve.model <- function(ev, vcv, addRestrs = NULL, addLimits = NULL, addEqRestrs = NULL,
                        addEqLimits=NULL, confun=NULL, shorts = FALSE, nport = 20, 
                        direction = 0, verbose = FALSE) {
  # Compute whole model
  #
  # inputs:
  # ev            N x 1 vector of expected returns
  # vcv           N x N covariance matrix of returns
  # addRestrs     each additional restriction is a Nx1 vector of 0s and 1s (the 1 is in the variable/s affected by the restriction)
  # addLimits     for every additional restriction we pass a number with the limit (>=)
  # shorts        logical, allow shorts is TRUE
  #
  # output is model object with the following elements
  # call				    original function call
  # gmv					global minimum variance portfolio
  # ulimit                global maximum value (return or cost) portfolio
  # llimit                global minimum value (return or cost) portfolio
  # ufrontier             frontier from GMV to upper limit portfolio
  # lfrontier             frontier from GMV to lower limit portfolio
  #
  
  call <- match.call()
  gmv.port <- gmv.portfolio(ev, vcv, addRestrs, addLimits, addEqRestrs, addEqLimits, confun, shorts)
  upper.frontier <- NULL
  lower.frontier <- NULL
  tryCatch({
    if (direction == 0 || direction == 1) {
      upper.frontier <- efficient.frontier(ev, vcv, 1, addRestrs, addLimits, addEqRestrs, addEqLimits, 
                                           confun, shorts, nport, gmv.port = gmv.port, verbose = verbose)
    }
    if (direction == -1 || direction == 0) {
      lower.frontier <- efficient.frontier(ev, vcv, -1, addRestrs, addLimits, addEqRestrs, addEqLimits, 
                                           confun, shorts, nport, gmv.port = gmv.port, verbose = verbose)
    }
  }, error = function(err){
    #errorHandler(err, call, NULL, NULL, NULL, NULL, NULL)
    stop(geterrmessage())
  })
  
  if (!is.null(lower.frontier)) {
    gmv.port <- list("call" = call,
                     "ev" = as.vector(lower.frontier$ev[1]),
                     "sd" = as.vector(lower.frontier$sd[1]),
                     "weights" = lower.frontier$weights[1,],
                     "HH" = sum(lower.frontier$weights[1,]^2))
  } else {
    gmv.port <- list("call" = call,
                     "ev" = as.vector(upper.frontier$ev[1]),
                     "sd" = as.vector(upper.frontier$sd[1]),
                     "weights" = upper.frontier$weights[1,],
                     "HH" = sum(upper.frontier$weights[1,]^2))
  }
  class(gmv.port) <- "portfolio"
  
  ans <- list("call" = call,
              "ufrontier" = upper.frontier,
              "lfrontier" = lower.frontier,
              "lfrontierLine" = SpatialLines(list(Lines(Line(cbind(lower.frontier$sd, lower.frontier$ev)), "mlf"))),
              "gmv" = gmv.port)
  class(ans) <- "model"
  ans
}

#
# print method for portfolio object
print.portfolio <- function(x, ...)
{
  cat("Call:\n")
  print(x$call, ...)
  cat("\nPortfolio expected cost:      ", format(x$ev, ...), "\n")
  cat("Portfolio standard deviation: ", format(x$sd, ...), "\n")
  cat("Portfolio weights:\n")
  print(round(x$weights,4), ...)
  
  #print(cbind(names(x$weights), round(x$weights, 4)))
  invisible(x)
}

#
# plot method for portfolio object
plot.portfolio <- function(object, pieChart=FALSE, main=NULL, xlab=NULL, ...)
{
  title = "Portfolio weights"
  x.lab = "Assets"
  if(!is.null(main)){
    title = main
  }
  if(!is.null(xlab)) {
    x.lab = xlab
  }
  asset.names <- names(object$weights)
  if (pieChart) {
    pie(object$weights, labels=asset.names, main=title, cex=0.6, radius=1, ...)
  } else if (!pieChart) {
    g = barplot(object$weights, names=asset.names,
                xlab=x.lab, ylab="Weight", main=title, 
                cex.names=0.6, ...)
    text(g, object$weights, sprintf("%1.2f%%", 100*object$weights), cex=0.6, pos =1)
  }
  invisible()
}

#
# print method for Markowitz object
print.Markowitz <- function(x, ...)
{
  cat("Call:\n")
  print(x$call)
  xx <- rbind(x$ev,x$sd)
  dimnames(xx)[[1]] <- c("ER","SD")
  cat("\nFrontier portfolios' expected costs and standard deviations\n")
  print(round(xx,4), ...)
  invisible(x)
}
# hard-coded 4, should let user control

#
# summary method for Markowitz object
summary.Markowitz <- function(object, risk.free=NULL)
{
  call <- object$call
  print(call)
  asset.names <- colnames(object$weights)
  port.names <- rownames(object$weights)
  if(!is.null(risk.free)) {
    # compute efficient portfolios with a risk-free asset
    nport <- length(object$ev)
    sd.max <- object$sd[1]
    sd.e <- seq(from=0,to=sd.max,length=nport)	
    names(sd.e) <- port.names
    
    #
    # get original er and cov.mat data from call 
    ev <- eval(call$ev)
    cov.mat <- eval(object$call$cov.mat)
    
    #
    # compute tangency portfolio
    tan.port <- tangency.portfolio(ev,cov.mat,risk.free)
    x.t <- sd.e/tan.port$sd		# weights in tangency port
    rf <- 1 - x.t			# weights in t-bills
    er.e <- risk.free + x.t*(tan.port$er - risk.free)
    names(er.e) <- port.names
    we.mat <- x.t %o% tan.port$weights	# rows are efficient portfolios
    dimnames(we.mat) <- list(port.names, asset.names)
    we.mat <- cbind(rf,we.mat) 
  }
  else {
    er.e <- object$ev
    sd.e <- object$sd
    we.mat <- object$weights
  }
  ans <- list("call" = call,
              "er"=er.e,
              "sd"=sd.e,
              "weights"=we.mat)
  class(ans) <- "summary.Markowitz"	
  ans
}

print.summary.Markowitz <- function(x, ...)
{
  xx <- rbind(x$er,x$sd)
  port.names <- names(x$er)
  asset.names <- colnames(x$weights)
  dimnames(xx)[[1]] <- c("ER","SD")
  cat("Frontier portfolios' expected costs and standard deviations\n")
  print(round(xx,4), ...)
  cat("\nPortfolio weights:\n")
  print(round(x$weights,4), ...)
  invisible(x)
}
# hard-coded 4, should let user control

plot.model <- function(object, plot.assets=TRUE, append=FALSE,
                       col="black",
                       main="Model summary",
                       xlim=NULL, ylim=NULL, 
                       ylab="Portfolio EV", xlab="Portfolio SD",
                       uflty=2, lflty=1, gmv.label="GMV", 
                       gmv.label.pos=4, ...) {
  # plot.assets   logical. If true then plot asset sd and ev
  
  call = object$call
  mu.vals = eval(call$ev)
  sd.vals = sqrt(diag(eval(call$vcv)))
  if (is.null(ylim)) {
    y.lim = range(c(0, mu.vals, object$ev))
  } else {
    y.lim = ylim
  }
  if(is.null(xlim)) {
    x.lim = range(c(0, sd.vals, object$sd))
  } else {
    x.lim = xlim
  }
  
  assets.already.plotted = FALSE
  
  if (append) {
    lines(object$lfrontier$sd, object$lfrontier$ev, 
          type="l", lwd=2, lty=lflty, col=col)
  } else {
    plot(object$lfrontier$sd, object$lfrontier$ev, 
         type="l", lwd=2, lty=lflty,
         xlim=x.lim, ylim=y.lim,
         xlab=xlab, ylab=ylab, 
         main=main, col=col, ...)
  }
  lines(object$ufrontier$sd, object$ufrontier$ev, type="l", lwd=2, lty=uflty, col=col)
  points(object$ufrontier$sd[1], object$ufrontier$ev[1], pch=23, bg="dimgray", col=col)
  text(object$gmv$sd, object$gmv$ev, labels=gmv.label, pos=gmv.label.pos, col=col)
  
  if (plot.assets & !assets.already.plotted) {
    points(sd.vals, mu.vals, pch=21, col="gray30")
    text(sd.vals, mu.vals, labels=rownames(mu.vals), pos=1, 
         cex=0.8, col="gray30")
    assets.already.plotted = TRUE
  }
  
  grid()
  
  invisible()
}

distanceToEff <- function(risk, value, model, direction = -1) {
  # Compute the distance from the given point to the efficient frontier given by the model
  # Returns a numeric vector with the risk distance and the value distance
  # Inputs:
  # risk    x coordinate of the point
  # value   y coordinate of the point
  # model   Markowitz model calculated
  # direction Distance to the lower frontier (-1 - default) or to the upper frontier (not implemented)
  
  # s. https://stackoverflow.com/questions/13758292/get-x-intercept-given-data-points-in-r
  
  if(direction == 1) {
    stop("Not implemented!!!")
    # SL1 <- SpatialLines(list(Lines(Line(cbind(model$ufrontier$sd, model$ufrontier$ev)), "muf")))
  }
  lineCollection <- Lines(apply(cbind(as.vector(risk), as.vector(value)), 1, function(x) { Line(rbind(0, x)) }), ID="a")
  SL2 <- SpatialLines(list(lineCollection))
  tryCatch(
    expr = {
      intersection <- coordinates(gIntersection(model$lfrontierLine, SL2))
      rdo = list()
      rdo$intersection <- intersection
      rdo$line <- coordinates(SL2)
      rdo$origin <- cbind(risk, value)
      i <- 1
      rdo$distance <- apply(rdo$intersection, 1, function(x) {
        rdo <- sqrt(sum((as.vector(rdo$origin[i,]) - as.vector(x))^2))
        i <- i + 1
        return (rdo)
      }) # TODO: No salen en orden
    }, 
    error = function(e) { 
      print(e$message)
    },
    finally = {
      if(!exists("rdo")) {
         rdo = list()
      }
      class(rdo) <- "dist2eff"
      return(rdo)
    }
  )
}


errorHandler <- function(err, call, x=NULL, A=NULL, B=NULL, 
                         Aeq=NULL, Beq=NULL, confun=NULL) {
  
  # file <- paste("C:\\temp\\", as.character(Sys.time(), format="%Y%m%d%H%M%S"), ".txt", sep="")
  # file.create(file)
  # fileCnn <- file(file)
  # writeLines(paste("Error message:", err$message), fileCnn)
  # writeLines(paste("Error call:", err$call), fileCnn)
  # writeLines(paste("Call:", call), fileCnn)
  # writeLines(paste("Traceback:", traceback()), fileCnn)
  # ...
  # close(fileCnn)
  
  message(err$message)
  message(err$call)
  print(call)
  cat("\n", "Traceback:", "\n")
  traceback()
  if (!is.null(x)) {
    cat("\n", "x vector:", x, "\n", "A matrix:", "\n")
  }
  if (!is.null(A)) {
    print(A, right=TRUE)
  }
  if (!is.null(B)) {
    cat("\n", "B vector:", B, "\n", "Aeq matrix:", "\n")
  }
  if (!is.null(Aeq)) {
    print(Aeq, right=TRUE)
  }
  if (!is.null(Beq)) {
    cat("\n", "Beq vector:", Beq, "\n")
  }
  if (!is.null(x) && !is.null(A) && !is.null(B)) {
    rdo <- A %*% x
    cat("\n", "Linear inequality constraints (<=0):", "\n", rdo<=B, "\n")
  }
  if (!is.null(x) && !is.null(Aeq) && !is.null(Beq)) {
    rdo <- Aeq %*% x
    cat("\n", "Linear equality constraints (=0):", "\n", rdo<=Beq, "\n")
  }
  if(!is.null(x) && !is.null(confun))  {
    rdo <- confun(x)
    cat("Non linear inequality constraints values (<=0):", rdo$c, "\n")
    cat("Non linear equality constraints values (=0):", rdo$ceq, "\n")
  }
  
  save(A, B, Aeq, Beq, file = "error.data")
  
  # addRestrWithLimits <- as.matrix(addLimits, ncol=1)
  # colnames(addRestrWithLimits)[1] <- "Limit"
  # addRestrWithLimits <- cbind(addRestr, addRestrWithLimits)
  # 
  # file <- paste("C:\\temp\\", as.character(Sys.time(), format="%Y%m%d%H%M%S"), ".xlsx", sep="")
  # write.xlsx(addRestrWithLimits, file, sheetName = "Constraints")
  # print(paste("Generated file with constraints:", file))
}
